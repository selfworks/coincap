import { Component, OnInit } from '@angular/core';
import { Assets, AssetsService } from '../../services/assets.service';
import { HttpParams } from '@angular/common/http';
import { WebsocketService } from '../../services/websocket.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.sass']
})
export class IndexComponent implements OnInit {
  public list: Assets[];

  constructor(private assetsService: AssetsService,
              private websocketService: WebsocketService) {
    this.websocketService.priceEvent.subscribe(prices => {
      if (prices) {
        for (const price in prices) {
          this.list.find(item => item.id === price).priceUsd = prices[price];
        }
      }
    });
  }

  ngOnInit() {
    this.getAssets();
  }

  private getAssets(): void {
    let params = new HttpParams();
    params = params.append('limit', '15');
    this.assetsService.get(params).subscribe(list => {
      this.list = list.data;
      this.websocketService.initSockets(this.list.map(item => item.id));
    });
  }
}
