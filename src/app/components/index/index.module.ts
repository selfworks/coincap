import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index.component';
import { MatCardModule } from '@angular/material';
import { AssetsService } from '../../services/assets.service';
import { WebsocketService } from '../../services/websocket.service';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule
  ],
  declarations: [IndexComponent],
  providers: [AssetsService, WebsocketService]
})
export class IndexModule { }
