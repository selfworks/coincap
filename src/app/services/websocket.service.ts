import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private pricesWs: WebSocket;
  public priceEvent: BehaviorSubject<any> = new BehaviorSubject(undefined);

  constructor() {
  }

  public initSockets(list: string[]): void {
    this.pricesWs = new WebSocket('wss://ws.coincap.io/prices?assets=' + list.join());

    this.pricesWs.onmessage = msg => {
      try {
        this.priceEvent.next(JSON.parse(msg.data));
      } catch (e) {
      }
    };
  }
}
