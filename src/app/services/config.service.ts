import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  static serviceUrl = 'https://api.coincap.io/v2/';

  constructor(private http: HttpClient) {
  }

  createHeaders(): HttpHeaders {
    return new HttpHeaders();
  }

  get<T>(url, params: HttpParams) {
    const headers = this.createHeaders();
    return this.http.get<T>(ConfigService.serviceUrl + url, { headers: headers, params: params });
  }

  post<T>(url, data) {
    const headers = this.createHeaders();
    return this.http.post<T>(ConfigService.serviceUrl + url, data, { headers: headers });
  }

  put<T>(url, data) {
    const headers = this.createHeaders();
    return this.http.put<T>(ConfigService.serviceUrl + url, data, { headers: headers });
  }

  delete<T>(url) {
    const headers = this.createHeaders();
    return this.http.delete<T>(ConfigService.serviceUrl + url, { headers: headers });
  }

}

export class Answer<T> {
  data: T[];
  timestamp: number;
}
