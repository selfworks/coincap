import { Injectable } from '@angular/core';
import { Answer, ConfigService } from './config.service';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AssetsService {

  constructor(private configService: ConfigService) { }

  get(params: HttpParams): Observable<Answer<Assets>> {
    return this.configService.get('assets', params);
  }
}

export class Assets {
  id: string;
  rank: string;
  symbol: string;
  name: string;
  supply: string;
  maxSupply: string;
  marketCapUsd: string;
  volumeUsd24Hr: string;
  priceUsd: string;
  changePercent24Hr: string;
  vwap24Hr: string;
}
